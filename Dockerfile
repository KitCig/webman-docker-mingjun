FROM php:8.3.0-cli-alpine3.18
# Container package  : mirrors.163.com、mirrors.aliyun.com、mirrors.ustc.edu.cn
RUN sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories
RUN cat /etc/issue
# Add basics first
RUN apk update && apk add bash curl ca-certificates openssl openssh git nano libxml2-dev tzdata icu-dev openntpd libedit-dev libzip-dev libjpeg-turbo-dev libpng-dev freetype-dev autoconf dpkg-dev dpkg file g++ gcc libc-dev make pkgconf re2c pcre-dev libffi-dev libressl-dev libevent-dev zlib-dev libtool automake supervisor gettext
RUN php -m
RUN docker-php-ext-install -j "$(nproc)" bcmath 
RUN docker-php-ext-install -j "$(nproc)" pcntl pdo_mysql

RUN curl -sS https://install.phpcomposer.com/installer | php && mv composer.phar /usr/local/bin/composer
RUN pecl install redis && docker-php-ext-enable redis


# Configure PHP
#COPY config/php.ini /usr/local/etc/php/conf.d/zzz_custom.ini

# Configure supervisord
#COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
#RUN chown -R nobody.nobody /run

# Setup document root
RUN mkdir -p /app

# Make the document root a volume
VOLUME /app
COPY ./ /app
#echo " > /usr/local/etc/php/conf.d/phalcon.ini
# Switch to use a non-root user from here on
USER root

# Add application
WORKDIR /app

# Expose the port nginx is reachable on
EXPOSE 28787
RUN cd /app
# Let supervisord start nginx & php
#CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
CMD ["php", "start.php", "start"]